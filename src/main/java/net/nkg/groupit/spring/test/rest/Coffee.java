package net.nkg.groupit.spring.test.rest;

import java.util.ArrayList;
import java.util.List;

public class Coffee {

    private String coffeeType;
    private List<String> ingredients = new ArrayList<>();

    public void setType(String type) {
        this.coffeeType = type;
    }

    public String getCoffeeType() {
        return this.coffeeType;
    }

    public void add(String ingredient) {
        this.ingredients.add(ingredient);
    }

    public List<String> getIngredients() {
        return this.ingredients;
    }

    @Override
    public String toString() {
        var result = "Your " + this.coffeeType + " contains: ";
        for (String i : this.ingredients) {
            result = result + i + ", ";
        }
        return result;
    }
}
