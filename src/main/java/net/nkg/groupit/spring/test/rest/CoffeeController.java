package net.nkg.groupit.spring.test.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CoffeeController {

    @RequestMapping("/whatsInside/{coffeeType}")
    public String coffee(@PathVariable String coffeeType)  {
        var coffee = new Coffee();

        if (coffeeType.equals("espresso")) {
            coffee.add("coffee");

        } else if (coffeeType.equals("cappuchino")) {
            coffee.add("coffee");
            coffee.add("milk");

        } else if (coffeeType.equals("iced")) {
            coffee.add("coffee");
            coffee.add("ice");

        } else if (coffeeType.equals("darkChocolateMocca")) {
            coffee.add("coffee");
            coffee.add("milk");
            coffee.add("cocoa powder");
        }
        return coffee.toString();
    }
}
